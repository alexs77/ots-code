#!/usr/local/bin/python3.7

# Winner, if X or O has 3 chars in row or col or diagonally
# so, if somebody has:
# 123, 456, 789
# 147, 258, 369
# 159, 357

def printBoard(board):
    print("/---+---+---\\")
    print("+ " + board[0] + " + " + board[1] + " + " + board[2] + " +")
    print("+---+---+---+")
    print("+ " + board[3] + " + " + board[4] + " + " + board[5] + " +")
    print("+---+---+---+")
    print("+ " + board[6] + " + " + board[7] + " + " + board[8] + " +")
    print("\\---+---+---/")

    return

def initBoard():
    """return an initialized board"""
    board = []
    for field in range(9):
        board.append("?")    
    return board

def main():
    # The "board" of the game, 3x3 = 9 elements, 0 to 8
    board = initBoard()
    printBoard(board)

if __name__ == '__main__':
    main()

# EOF